# Mini Project 2

Necessary ROS packages for MAE/ECE 5930 Mini Project 2 requirements. 

**MINI-PROJECT 2 REPORT
Locate, Acquire, Project Robot (LAPbot)**

**MAE/ECE 5930 - Mobile Robotics | March 2, 2019 |**
**Jordan Cline, Nicholas Huld, Brandon Kenison, Jeremy Pickering**


# Execution Instructions

**Create a new workspace**
*  `mkdir -p [name of new ws]/src`
*  `cd [name of new ws]/src`
 
**Copy lapBot_launch.rosinstall to the `~/src/[name of new ws]/src` directory**

**Use wstool**
*  `wstool init`
*  `wstool merge lapBot_launch.rosinstall`
*  `wstool up`

**Build your workspace**
*  `cd ~/src/[name of new ws]`
*  `catkin_make`

**Source your workspace**
*  `source ~/src/[name of new ws]/devel/setup.bash`

**Run the sim**
*  `roslaunch turtlebot_launch lapBot.launch`

Rviz will start and the visualization of the lapBot will initialize. The robot is commanded using the ‘W,’ ‘X,’ ‘A,’ and ‘D’ keys on the keyboard. ‘W’ is positive left wheel acceleration, ‘X’ is negative left wheel acceleration. ‘A’ is positive right wheel acceleration, ‘D’ is negative right wheel acceleration. The dynamics implemented is an improved, smooth differential drive.

# Moving Vehicle

The robot moves according to its own reference frame using differential drive dynamics.  The robot is controlled using a keyboard. The ‘W’ key controls the forward acceleration of the left wheel. The ‘A’ key controls the forward acceleration of the right wheel. Similarly the ‘X’ key decelerates the left wheel and the ‘D’ key decelerates the right wheel.  

# Updated Dynamics

We chose to use the smooth differential drive model. Equations are shown below.

x'= r2l+rcos
'l=ul
y' = r2l+rsin
'r=ur
' = rLr-l





If we were to build this bot we would choose a differential drive. The smooth differential drive model was implemented by assigning the keyboard input to be the rotational accelerations of the left and right wheels. We used the Euler method to integrate our rotational accelerations to rotational velocities. The rotational velocities, wheel radius (r), and distance from wheel center to center (L) were input to the equations above. We used the Euler method again to update the robot’s x-y position and orientation. 

# Moving Visualization

We reused the visualization we created from mini-project 1.
